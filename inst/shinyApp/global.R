
source("keybind_functions.R")
source("default_keybindings.R")
source("cache_functions.R")
source("logging.R")
source("alerts.R")
source("update_app.R")

gvPackageName <- "OpenMFD"

# Create folder for persistent data
cachePath <- file.path("~", ".Rcache", gvPackageName)
dir.create(cachePath, showWarnings = FALSE, recursive = TRUE)
