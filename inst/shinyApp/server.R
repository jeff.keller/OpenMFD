
server <- function(input, output, session) {
  
  # Reactive conductors
  rv <- reactiveValues(
    keysLocked = FALSE,
    kb_profile_mtime = NULL,
    keybindings = kb_default,
    kb_profiles = extractKeybindingsProfileName(getKeybindingsFiles())
  )
  
  # Watch for keybindings profile *selection* changes
  observe({
    
    kb_profile <- input$kb_profile
    use_custom_profile <- input$use_custom_profile
    
    if (use_custom_profile & kb_profile != "") {
      
      # Load the profile
      OpenMFD::msg("Loading profile: ", kb_profile)
      rv$keybindings <- getKeybindings(kb_profile)
      rv$kb_profile_mtime <- getKeybindingsModTime(kb_profile)
      
      # Update the active profile header
      output$active_profile <- renderText(paste("Active Profile:", kb_profile))
      
    }
    
    if (!use_custom_profile) {
      
      # Update the active profile header
      output$active_profile <- renderText(paste("Active Profile: default controls"))
      
    }
    
    # Persist the selection on disk
    write_cache_value(kb_profile, name = "kb_profile")
    
    
    
  })
  
  # Watch for checkbox changes indicating whether to use custom profiles
  observe({
    
    use_custom_profile <- input$use_custom_profile
    
    if (!use_custom_profile) {
      
      OpenMFD::msg("Restoring default keybindings")
      rv$keybindings <- kb_default
      
    }
    
    write_cache_value(use_custom_profile, name = "use_custom_profile")
    
  })
  
  # Watch for keybindings profile *file* changes
  observe({

    kb_profile <- isolate(input$kb_profile)
    use_custom_profile <- isolate(input$use_custom_profile)
    invalidateLater(3000, session = NULL)

    if (!kb_profile == "" & use_custom_profile) {

      kb_profile_mtime <- getKeybindingsModTime(kb_profile)

      # Check if the selected profile's file has changed
      if (!identical(isolate(rv$kb_profile_mtime), kb_profile_mtime)) {

        # If the file exists, reload it
        # Otherwise, use the default keybindings
        kb_file <- getKeybindingsFile(kb_profile)
        if (!is.na(kb_file)) {

          OpenMFD::msg("Reloading profile: ", kb_profile)
          rv$keybindings <- getKeybindings(kb_profile)

        } else {

          OpenMFD::msg("Profile changed, restoring defaults")
          rv$keybindings <- kb_default

        }

        rv$kb_profile_mtime <- kb_profile_mtime

      }

    }

  })
  
  # Check if the list of available profiles has changed
  observe({
    
    invalidateLater(3000, session = NULL)
    kb_profiles <- extractKeybindingsProfileName(getKeybindingsFiles())
    
    if (!identical(kb_profiles, rv$kb_profiles)) {
      
      OpenMFD::msg("Updating list of available profiles")
      kb_profile <- isolate(input$kb_profile)
      if (!kb_profile %in% kb_profiles) kb_profile <- NULL
      shiny::updateSelectInput(
        session = session, inputId = "kb_profile",
        choices = kb_profiles, selected = kb_profile
      )
      rv$kb_profiles <- kb_profiles
      
    }
    
  })
  
  # Watch for button presses
  lapply(seq_along(kb_default), function(i) {
    
    # Create an observer for each key
    action <- names(kb_default)[i]
    
    observeEvent(input[[action]], ignoreInit = TRUE, handlerExpr = {
      
      ui_value <- input[[action]]
      keysLocked <- isolate(rv$keysLocked)
      
      key <- names(kb_default)[i] # TODO: not sure using the default names is the best idea here...
      keybinding <- isolate(rv$keybindings)[[action]]
      
      if (is.null(ui_value)) {
        
        warning("Keybinding found (", action, ") but there is no corresponding UI element.", call. = FALSE, immediate. = TRUE)
        
      } else if (is.na(keybinding$key)) {
        
        # Tell the user that this action is unbound
        alert_unbound_key(action)
        
      } else if (ui_value > 0 & !keysLocked) {
        
        activateKeybinding(keybinding, action)
        
      }
      
    })
    
  })
  
  # Stop the application
  shiny::observe({
    if (input$stop_app > 0) {
      OpenMFD::msg("Shutting down...")
      shiny::stopApp()
    }
  })
  
  # Lock keys
  shiny::observe({
    
    if (input$lock > 0) {
      
      if (input$lock %% 2) {
        OpenMFD::msg("Locking keys")
        rv$keysLocked <- TRUE
      } else {
        OpenMFD::msg("Unlocking keys")
        rv$keysLocked <- FALSE
      }
      
    }
    
  })
  
  # Update lock button text
  shiny::observe({
    
    if (rv$keysLocked) {
      
      shiny::updateActionButton(
        session = session, inputId = "lock",
        label = "Unlock", icon = shiny::icon("lock")
      )
      
    } else {
      
      shiny::updateActionButton(
        session = session, inputId = "lock",
        label = "Lock", icon = shiny::icon("unlock")
      )
      
    }
    
  })
  
  # Check for package updates every 30 minutes
  observe({
    
    invalidateLater(1800000, session = NULL)
    
    if (updateAvailable()) {
      OpenMFD::msg("An update is available! ", appVersion(), " --> ", getLatestVersion())
    }
    
  })
  
}
